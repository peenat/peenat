<!---
Hi, thank you so much for thinking about a new command!
Please make sure no one else featured this.
-->

**Name**
The name of the command

**Purpose**
Why should this command be added?

**What does it do?**
What will the command do?

**Example usage**
Examples of the command being used
