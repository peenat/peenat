<!---
Hi, thank you so much for reporting an issue!
Please make sure there is no issue already about this.
-->

<!---
Either
Command name / None
-->

**Command name**
The name of the command

**To Reproduce**
Steps to reproduce the behavior:

1. Type in .help
2. See that it says ...

**Expected behavior**
A clear and concise description of what you expected to happen.

**Actual behavior**
A clear and concise description of what actually happened.

**Screenshots**
If applicable, add screenshots to help explain your problem.

<!---
You can get the version by running .botinfo
-->

**Version**

-   Version [e.g. 22]

**Additional context**
Add any other context about the problem here.
