# Peenat

![Peenat banner](peenat-banner.png)

Peenat is a Discord bot.
It is still very much in development, currently not ready for production use.

Due to some age-related Discord policies, we will not be able to launch Peenat.

We're looking for partners to advertise Peenat,
and testers to experiment with new features, and help us out with ideas.

## If you're interested

Check out the project [here](https://github.com/werymanen/peenat/projects/2)

Contact us on Discord [here](https://discord.gg/PxqUyYx)
