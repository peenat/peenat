const defaLog = global.console.log;
global.console.log = (...stuff) => {
    defaLog("MASTER", ...stuff);
};

const { ShardingManager } = require("discord.js");

const manager = new ShardingManager("./src/bot.js", {
    token: process.env.TOKEN || require("../token.json").token
});

manager.spawn();
manager.on("shardCreate", shard => {
    console.log("Launched shard " + shard.id);

    shard.on("ready", () => {
        console.log("Shard " + shard.id + " is ready");

        shard.send({ type: "shardId", data: { shardId: shard.id } });
    });
});
