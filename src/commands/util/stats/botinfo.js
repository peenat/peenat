const fs = require("fs").promises;
const package = require("../../../../package.json");

async function botinfo(args, msg) {
    const uptime = new Date(Date.now() - startTime).getMinutes() + " minutes";
    const savesSize = (await fs.stat(save.file)).size / 1000000.0 + "MB";
    const commands = Object.keys(command.commands).length;
    const cats = Object.keys(command.cats).length - 1; // Exclude category all
    const guilds =
        bot.guilds.cache.array().length +
        "/" +
        (await bot.shard.fetchClientValues("guilds.cache.size"));
    const channels =
        bot.channels.cache.array().length +
        "/" +
        (await bot.shard.fetchClientValues("channels.cache.size"));
    const version = package.version;

    msg.channel.send(
        createEmbed(
            "Bot info",
            "Stats",
            {
                name: "Uptime",
                value: uptime
            },
            {
                name: "Saves' size",
                value: savesSize
            },
            {
                name: "Commands",
                value: commands
            },
            {
                name: "Categories",
                value: cats
            },
            {
                name: "Guilds",
                value: guilds
            },
            {
                name: "Channels",
                value: channels
            },
            {
                name: "Version",
                value: version
            }
        )
    );
}

module.exports = {
    name: "Bot info",
    desc: "Some information about Peenat",
    help: [
        {
            cmd: "",
            desc: "Some information about Peenat"
        }
    ],
    aliases: ["peenat", "bot"],
    cmd: botinfo
};
