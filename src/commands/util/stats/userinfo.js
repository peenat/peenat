function userinfo(args, msg) {
    var user = null;
    if (msg.mentions.members.array()[0]) {
        var a = args[0].substring(2, args[0].length - 1);

        if (a.startsWith("!")) a = a.substring(1);

        user = bot.users.resolve(a);
    }
    if (args[0] && !args[0].startsWith("<"))
        user = bot.shard.broadcastEval("this.users.resolve(" + args[0] + ")");

    if (!user) return msg.channel.send("Could not resolve user");

    var guildMember;

    try {
        guildMember = msg.guild.member(user);
    } catch (err) {}

    const customStatus = user.presence.activities.map(a => {
        if (a.type == "CUSTOM_STATUS") return a;
    })[0];

    const otherStatus = user.presence.activities.filter(s => {
        return s.type != "CUSTOM_STATUS";
    });

    const registerDate = user.createdAt.toDateString();
    const tag = user.tag;
    const id = user.id;
    const status = customStatus
        ? customStatus.emoji
            ? customStatus.emoji.name
            : "" + " " + customStatus.state
        : "No status";
    const game = otherStatus
        .map(a => {
            if (!a) return;
            return (
                a.type.toUpperCase().split("")[0] +
                a.type.toLowerCase().split("").splice(1).join("") +
                " " +
                a.name
            );
        })
        .join(", ");

    var roles = "Not a member";
    var joinDate = "Not a member";

    if (guildMember) {
        roles = guildMember.roles.cache
            .array()
            .map(a => {
                if (a.name != "@everyone") return a.name;
                else return "everyone";
            })
            .join(", ");

        joinDate = guildMember.joinedAt.toDateString();
    }

    msg.channel.send(
        createEmbed(
            "User Info",
            "Information about " + user.username,
            {
                name: "Register Date",
                value: registerDate
            },
            {
                name: "Tag",
                value: tag
            },
            {
                name: "Id",
                value: id
            },
            {
                name: "Status",
                value: status
            },
            {
                name: "Game",
                value: game || "None"
            },
            {
                name: "Roles",
                value: roles
            },
            {
                name: "Join Date",
                value: joinDate
            }
        )
    );
}

module.exports = {
    name: "User info",
    desc: "Some info about a user",
    help: [
        {
            cmd: "",
            desc: "Some information about you"
        },
        {
            cmd: "<@Name>",
            desc: "Some information about the given user"
        },
        {
            cmd: "<id>",
            desc: "Some information about the given user"
        }
    ],
    aliases: ["user", "about"],
    cmd: userinfo
};
