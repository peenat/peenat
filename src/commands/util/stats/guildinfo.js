function guildinfo(args, msg) {
    var guild;
    if (args[0])
        if (bot.guilds.resolve(args[0])) guild = bot.guilds.resolve(args[0]);
        else return msg.channel.send("I am not in that guild, sadly");
    else guild = msg.guild;

    const name = guild.name;
    const id = guild.id;
    const owner = guild.ownerID;
    const region = guild.region;
    const tier = guild.premiumTier;
    const explicit = guild.explicitContentFilter;
    const verification = guild.verificationLevel;
    const creationDate = new Date(guild.joinedTimestamp).toDateString();
    const roles = guild.roles.cache.array().length;
    const emojis = guild.emojis.cache.array().length;
    const members = guild.members.cache.array().filter(m => {
        if (m.user.bot) return false;
        else return true;
    }).length;
    const bots = guild.members.cache.array().filter(m => {
        if (m.user.bot) return true;
        else return false;
    }).length;
    const text = guild.channels.cache.array().filter(c => {
        if (
            c instanceof Discord.CategoryChannel ||
            c instanceof Discord.VoiceChannel
        )
            return false;
        else return true;
    }).length;
    const voice = guild.channels.cache.array().filter(c => {
        if (
            c instanceof Discord.CategoryChannel ||
            c instanceof Discord.TextChannel
        )
            return false;
        else return true;
    }).length;

    msg.channel.send(
        createEmbed(
            "Guild information",
            "Information about " + name,
            { name: "Name", value: name || "No name" },
            { name: "ID", value: id || "No id" },
            { name: "Owner", value: owner || "No owner" },
            { name: "Region", value: region || "No region" },
            { name: "Tier", value: tier || "No tier" },
            { name: "Explicit", value: explicit || "No explicit" },
            { name: "Verification", value: verification || "No verification" },
            {
                name: "Creation date",
                value: creationDate || "No creation date"
            },
            { name: "Roles", value: roles || "No roles" },
            { name: "Emojis", value: emojis || "No emojis" },
            { name: "Members", value: members || "No members" },
            { name: "Bots", value: bots || "No bots" },
            { name: "Text channels", value: text || "No text" },
            { name: "Voice channels", value: voice || "No voice" }
        )
    );
}

module.exports = {
    name: "Guild info",
    desc: "Guild information",
    help: [
        {
            cmd: "",
            desc: "Information about current guild"
        },
        {
            cmd: "<id>",
            desc: "Information about a given guild"
        }
    ],
    aliases: "guild",
    cmd: guildinfo
};
