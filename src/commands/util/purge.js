function purge(args, msg) {
    if (args[0] <= 100) msg.channel.bulkDelete(args[0]);
    else {
        const fTimes = args[0] / 100;
        const times = parseInt(fTimes);
        const extra = parseInt((fTimes - times) * 100) + 1;

        for (let i = 0; i < times; i++) {
            msg.channel.bulkDelete(100);
        }

        if (extra) msg.channel.bulkDelete(extra);
    }
}

module.exports = {
    name: "Purge",
    desc: "Deletes messages",
    help: [
        {
            cmd: "<amount>",
            desc: "Deletes a specific amount of messages"
        }
    ],
    aliases: ["delete"],
    cmd: purge
};
