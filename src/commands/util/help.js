const {
    run,
    commands,
    cats,
    getNameOfCommand,
    getDescOfCommand,
    getNameOfCat,
    getDescOfCat
} = command;

const { prefix } = config;

function help(args, msg, guild) {
    if (!args[0]) help();
    else if (commands[args[0]]) helpCmd();
    else if (cats[args[0]]) helpCat();
    else return msg.channel.send("No command or category called " + args[0]);

    function help() {
        msg.channel.send(
            createEmbed(
                "Help",
                "Categories",
                ...Object.keys(cats).map(c => {
                    return {
                        name: getNameOfCat(c),
                        value: getDescOfCat(c)
                    };
                }),
                {
                    name: "Get help using",
                    value: `${prefix}help <category>`
                }
            )
        );
    }

    function helpCmd() {
        if (!commands[args[0]]) return msg.channel.send("No command like that");

        if (!commands[args[0]].help)
            return msg.channel.send("No help defined for " + args[0]);

        msg.channel.send(
            createEmbed(
                getNameOfCommand(args[0]),
                getDescOfCommand(args[0]),
                ...commands[args[0]].help.map(c => {
                    return {
                        name: `${prefix}${args[0]} ${c.cmd}`,
                        value: c.desc
                    };
                })
            )
        );
    }

    function helpCat() {
        if (!cats[args[0]]) return msg.channel.send("No category like that");

        msg.channel.send(
            createEmbed(
                getNameOfCat(args[0]),
                getDescOfCat(args[0]),
                ...Object.entries(cats[args[0]])
                    .filter(c => {
                        if (!c[1].cmd && !c[1].alias) return false;
                        else return true;
                    })
                    .map(c => {
                        return {
                            name: getNameOfCommand(c[0]),
                            value: getDescOfCommand(c[0])
                        };
                    })
            )
        );
    }
}

module.exports = {
    id: "help",
    name: "Help",
    desc: "Help message",
    help: [
        {
            cmd: "",
            desc: "Lists all commands"
        },
        {
            cmd: "<cmd>",
            desc: "Help for a specific command"
        },
        {
            cmd: "<category>",
            desc: "Help for a specific category"
        }
    ],
    cmd: help
};
