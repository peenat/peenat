function commandCalled(args, msg, guild, user) {
    if (msg.deletable) msg.delete();

    if (args.length != 0) {
        msg.channel.send(args.raw.join(" "));
        return;
    }

    user.isMorph = !user.isMorph;
}

function say(msg) {
    if (msg.deletable) msg.delete();

    msg.channel.send(msg.content);
}

bot.on("message", msg => {
    const saves = save.getUserSave(msg.author.id);

    if (
        msg.content.startsWith(config.prefix) ||
        msg.content.startsWith(save.getGuildSave(msg.channel.guild.id).prefix)
    )
        return;

    if (saves.isMorph) say(msg);
});

module.exports = {
    name: "Morph",
    desc: "Say things as Peenat",
    help: [
        {
            cmd: "",
            desc: "Toggles morph mode"
        },
        {
            cmd: "...<text>",
            desc: "Uses morph mode"
        }
    ],
    aliases: ["broadcast", "bc"],
    cmd: commandCalled
};
