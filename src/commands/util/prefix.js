function prefix(args, msg, saves) {
    saves.prefix = args[0];
    msg.channel.send("Made prefix " + args[0]);
}

module.exports = {
    name: "Prefix",
    desc: "Set the prefix",
    help: [{ cmd: "<prefix>", desc: "Set the prefix" }],
    aliases: "setprefix",
    cmd: prefix
};
