async function err(args) {
    throw new Error(args.raw.join(" ") || "Test error");
}

module.exports = {
    name: "Test error",
    desc: "Test error for development purposes",
    aliases: "error",
    cmd: err
};
