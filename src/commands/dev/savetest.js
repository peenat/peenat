function savetest(args, msg, guildConfig, userConfig) {
    if (args[0] == "set")
        if (args[1] == "guild") return (guildConfig.test = "value");
        else return (userConfig.test = "value");
    else if (args[0] == "get")
        if (args[1] == "guild")
            return msg.channel.send(guildConfig.test || "No value");
        else return msg.channel.send(userConfig.test || "No value");
}

module.exports = {
    name: "Test save",
    desc: "Test save command",
    help: [
        {
            cmd: "set <guild/user>",
            desc: "Sets test save for the guild/user"
        },
        {
            cmd: "get <guild/user>",
            desc: "Gets test save for the guild/user"
        }
    ],
    aliases: "save",
    cmd: savetest
};
