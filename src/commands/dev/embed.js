function embed(args, msg) {
    var data;

    if (args[0] == "json") {
        data = JSON.parse(args.raw.slice(1).join(" "));
    } else {
        data = {
            title: args[0] || "Untitled embed",
            desc: args[1] || "Unknown",
            fields: []
        };

        const forl = (args.length - 2) / 2;

        for (let i = 0; i < forl; i++) {
            const pair = {
                name: args[2 + i * 2] || "Empty name",
                value: args[2 + i * 2 + 1] || "Empty value"
            };

            data.fields.push(pair);
        }

        data.fields.push();
    }

    if (msg.deletable) msg.delete();
    msg.channel.send(createEmbed(data.title, data.desc, ...data.fields));
}

module.exports = {
    name: "Embed creator",
    desc: "Easily create embeds using JSON",
    help: [
        {
            cmd: "<title> <desc> [name1] [value1] [name2] [value2] ...",
            desc: "Creates an embed using JSON"
        },
        {
            cmd: "json <json>",
            desc: "Creates an embed using only JSON"
        }
    ],
    cmd: embed
};
