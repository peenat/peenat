function invite(args, msg) {
    if (!args[0] || args[0] == "perm") msg.channel.send(config.invite.perm);
    else if (args[0] == "noperm" || args[0] == "noPerm")
        msg.channel.send(config.invite.noPerm);
    else msg.channel.send("Usage: " + config.prefix + "invite [perm / noperm]");
}

module.exports = {
    name: "Invite",
    desc: "Invite Peenat to your guild",
    help: [
        {
            cmd: "",
            desc: "Invite link with default permissions"
        },
        {
            cmd: "perm",
            desc: "Invite link with default permissions"
        },
        {
            cmd: "noperm",
            desc: "Invite link with no permissions"
        }
    ],
    aliases: "get",
    cmd: invite
};
