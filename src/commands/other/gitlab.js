function gitlab(args, msg) {
    msg.channel.send(
        "Source code available on Gitlab: https://gitlab.com/manen/peenat"
    );
}

module.exports = {
    name: "Gitlab",
    desc: "Open source repository of Peenat",
    aliases: ["source", "sourcecode", "github"],
    cmd: gitlab
};
