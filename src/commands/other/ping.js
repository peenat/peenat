function ping(args, msg) {
    msg.channel.send("Pong! :ping_pong:");
}

module.exports = {
    name: "Ping",
    desc: "You know what it does",
    cmd: ping
};
