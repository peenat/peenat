function credits(args, msg) {
    msg.channel.send(
        createEmbed(
            "Credits and Partners",
            "They're nice <3",
            {
                name: "Hunor_mc",
                value: "for making an awesome server to develop in"
            },
            {
                name: "manen",
                value: "for writing most of the actual bot"
            }
        )
    );
}

module.exports = {
    name: "Credits",
    desc: "List of people who helped make Peenat possible",
    cmd: credits
};
