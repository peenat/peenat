function activity(args, msg) {
    msg.channel.send(global.activity.random());
}

module.exports = {
    name: "Activity",
    desc: "Gives you a random activity",
    cmd: activity
};
