const { memeAsync } = require("memejs");

async function meme(args, msg) {
    const memeMsg = await msg.channel.send(
        `Getting your spicy meme${args[0] ? ` from ${args[0]}` : ""}...`
    );
    const meme = await memeAsync(args.join(" "));

    memeMsg.edit(
        `${meme.title}
by ${meme.author} in ${meme.subreddit} subreddit
${meme.url}`
    );
}

module.exports = {
    name: "Meme",
    desc: "Gives you soup memes",
    help: [
        {
            cmd: "",
            value: "Gives you a meme from a random subreddit"
        },
        {
            cmd: "<subreddit>",
            value: "Gives you a meme from a specific subreddit"
        }
    ],
    aliases: "reddit",
    cmd: meme
};
