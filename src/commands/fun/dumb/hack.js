async function hack(args, hackMsg) {
    const target = hackMsg.mentions.users.array()[0];
    const name = target.username;

    const firstMsg = `Hacking ${name}`;
    const msg = await hackMsg.channel.send(firstMsg + ".");

    for (let i = 0; i < parseInt(Math.random() * 2); i++) {
        await sleep(50 + Math.random() * 150);
        msg.edit(firstMsg + ".");
        await sleep(50 + Math.random() * 150);
        msg.edit(firstMsg + "..");
        await sleep(50 + Math.random() * 150);
        msg.edit(firstMsg + "...");
    }

    await sleep(500);
    msg.edit(`Hacking ${name}'s fortnite account`);

    await sleep(1000);
    msg.edit(`Stealing all of ${name}'s vbux`);

    await sleep(250);
    msg.edit("Hacking done");
}

function sleep(f) {
    return new Promise(res => {
        setInterval(res, f);
    });
}

module.exports = {
    name: "Hack",
    desc: "Hack the blockchain server running HTML",
    help: [
        {
            cmd: "<@user>",
            value: "Hack the HTML blockchain bitcoin"
        }
    ],
    cmd: hack
};
