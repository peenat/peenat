function pp(args, msg, guildConfig, userConfig) {
    if (args[0] == "get" || !args[0]) {
        if (!userConfig.pp) userConfig.pp = {};
        if (!userConfig.pp.size)
            userConfig.pp.size = parseInt(Math.random() * 13);

        const size = userConfig.pp.size;

        var value = "8";

        for (let i = 0; i < size; i++) {
            value += "=";
        }

        value += "D";

        msg.channel.send(
            createEmbed(
                "pp",
                "your pp size",
                {
                    name: "Your pp:",
                    value
                },
                {
                    name: "Size:",
                    value: size + " inches"
                }
            )
        );
    }

    if (args[0] == "reset") {
        if (!userConfig.pp) userConfig.pp = {};

        userConfig.pp.size = Math.floor(Math.random() * 13);

        msg.channel.send("New pp size: " + userConfig.pp.size);
    }
}

module.exports = {
    name: "pp",
    desc: "pp size measure",
    help: [
        {
            cmd: "",
            desc: "Same as pp get"
        },
        {
            cmd: "get",
            desc: "Gets you your pp size"
        },
        {
            cmd: "reset",
            desc: "Resets your pp size"
        }
    ],
    aliases: ["penis", "dick"],
    cmd: pp
};
