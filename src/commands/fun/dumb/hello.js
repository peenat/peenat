function hello(args, msg) {
    if (args[0] != "there") return msg.channel.send("Hi! :wave:");
    else return msg.channel.send("GENERAL KENOBI :heart:");
}

module.exports = {
    name: "Hello",
    desc: "Welcome! :wave:",
    cmd: hello
};
