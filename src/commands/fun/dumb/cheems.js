function cheems(args, msg) {
    const amount = args[0] || 1;

    if (amount > 6)
        return msg.channel.send(
            `Ooh, hold up! That's ${
                amount > 12 ? "way " : ""
            }too many calories!`
        );

    if (msg.deletable) msg.delete();
    msg.channel.send(
        "https://media.discordapp.net/attachments/667076300611256341/738736649478995988/dogyum-1.gif"
    );
    for (let i = 0; i < amount; i++) {
        msg.channel.send(
            "https://media.discordapp.net/attachments/667076300611256341/738736685587890176/dogyummi-1.gif"
        );
    }
}

module.exports = {
    name: "Cheems",
    desc: "cheemsburger",
    help: [
        {
            cmd: "",
            desc: "Cheemsburger with a y not a z you idiot"
        },
        {
            cmd: "<amount>",
            desc: "Eat more and be fat"
        }
    ],
    aliases: ["burger", "eat"],
    cmd: cheems
};
