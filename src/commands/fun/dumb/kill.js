const msgs = [
    "%u died to Donald Trump",
    "Turns out %u is dead",
    "%u used Windows",
    "%u accidentally restarted the USSR",
    "%u forgot how to breathe"
];

function kill(args, msg) {
    return msg.channel.send(
        msgs[Math.floor(Math.random() * msgs.length)].replace(
            /%u/g,
            msg.mentions.members.array()[0].user.username
        )
    );
}

module.exports = {
    name: "Kill",
    desc: "Murder people",
    help: [
        {
            cmd: "<mention>",
            desc: "Kill someone"
        }
    ],
    aliases: "murder",
    cmd: kill
};
