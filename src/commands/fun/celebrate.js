async function celebrate(args, msg) {
    const text = args.raw.join(" ");

    msg.channel.send(
        createEmbed("Celebration", "Let's celebrate :tada:\n\n**" + text + "**")
    );

    if (msg.deletable) msg.delete();
}

module.exports = {
    name: "Celebrate",
    desc: "Celebrate something :tada:",
    aliases: ["celebration", "yay"],
    help: [
        {
            cmd: "...<something>",
            desc: "Celebrate something :tada:"
        }
    ],
    cmd: celebrate
};
