const texts = [
    "You'll get through this.",
    "I believe in you.",
    "Good job.",
    "These are tough times.",
    "I'm sure you'll be fine",
    "I'm very proud of you.",
    "I hope you succeed.",
    "Finally, some good news.",
    "I'm very sad to hear that.",
    "That's very good.",
    "Great!",
    "I'm sorry."
];

function therapy(args, msg) {
    if (args[0] != "please")
        msg.channel.send(texts[Math.floor(Math.random() * texts.length)]);
    else
        msg.channel.send(
            "Mental issues are very serious. Take them seriously, and talk with a mental doctor about it. We hope you'll repair from your issues. :heart:"
        ); // <3
}

module.exports = {
    name: "Therapy",
    desc: "Therapy time",
    help: [
        {
            cmd: "",
            desc: "Not actual therapy"
        },
        {
            cmd: "please",
            desc: "Some actual advice"
        }
    ],
    aliases: "therapytime",
    cmd: therapy
};
