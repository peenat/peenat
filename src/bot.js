const startTime = Date.now();

const Discord = require("discord.js");
const bot = new Discord.Client();

globalize();

function createEmbed(title, desc, ...fields) {
    return new Discord.MessageEmbed()
        .setTitle(title)
        .setDescription(desc)
        .addFields(fields)
        .setColor(0xc87722)
        .setFooter("Made with ❤️")
        .setTimestamp();
}

process.on("message", msg => {
    if (!msg.type) return false;

    if (msg.type == "shardId") {
        bot.shard.id = msg.data.shardId;
        console.log("My shard id is: " + bot.shard.id);
    }
});

function globalize() {
    global.createEmbed = createEmbed;

    global.startTime = startTime;

    global.Discord = Discord;

    global.bot = bot;

    const defaLog = global.console.log;
    global.console.log = (...stuff) => {
        defaLog(bot.shard.id || "", ...stuff);
    };

    global.config = require("../config.json");

    global.activity = require("./activity");
    global.command = require("./command");
    global.save = require("./save");
}

bot.on("ready", () => {
    console.log(
        `${bot.user.tag} logged in at ${bot.guilds.cache.array().join(", ")}.`
    );

    activity.change(bot);

    setInterval(() => {
        activity.change(bot);
    }, 60000);
});

bot.on("message", msg => {
    if (msg.author.bot) return;

    const userSave = save.getUserSave(msg.author.id);

    if (msg.channel == msg.author.dmChannel) {
        if (!userSave.firstDmYet) {
            msg.channel.send(
                "You just slid into my DMs. Nice job, but to have any functionality out of me, you have to use me in a server. Invite link: \n" +
                    config.invite.perm +
                    " with automatic permissions and \n" +
                    config.invite.noPerm +
                    " without them."
            );
            userSave.firstDmYet = true;
        } else {
            msg.channel.send(
                "You still have to invite me. \n" +
                    config.invite.perm +
                    " with automatic permissions and \n" +
                    config.invite.noPerm +
                    " without them."
            );
        }
        return;
    }

    const guildSave = save.getGuildSave(msg.guild.id);

    if (
        msg.content.startsWith(config.prefix) ||
        msg.content.startsWith(guildSave.prefix)
    ) {
        command.run(msg, guildSave, userSave);
    }
});

bot.login(process.env.TOKEN || require("../token.json").token);
