const arrgv = require("arrgv");

const { prefix } = config;
const fs = require("fs").promises;
const path = require("path");

const cmdLimit = 500;

const commands = {};

const cats = {
    all: { ...commands, name: "All", desc: "All commands" }
};

function getNameOfCommand(command) {
    var aliasWarn = "";
    if (commands[command].alias)
        aliasWarn = " | alias of " + commands[command].alias;
    if (commands[command].aliases) {
        const aliasesArray =
            typeof commands[command].aliases == "object"
                ? commands[command].aliases
                : [commands[command].aliases];
        aliasWarn = ` | ${aliasesArray.join(", ")}`;
    }
    return `${prefix}${command}${aliasWarn}`;
}

function getDescOfCommand(command) {
    if (commands[command].alias) return commands[commands[command].alias].desc;
    else return commands[command].desc;
}

function getNameOfCat(cat) {
    var raw = " | " + cat;
    if (!cats[cat].name) raw = "";
    return `${cats[cat].name || cat}${raw}`;
}

function getDescOfCat(cat) {
    return `${cats[cat].desc || cat}`;
}

function load(obj, catDesc) {
    const name = obj.id;
    const cat = obj.cat;

    cats[cat] = cats[cat] || catDesc || {};

    commands[name] = obj;
    cats[cat][name] = obj;
    cats.all[name] = obj;

    if (obj.aliases)
        if (typeof obj.aliases == "object") alias(name, ...obj.aliases);
        else alias(name, obj.aliases);
}

async function loadAll() {
    const catDescName = ".cat.json";

    const promises = [];

    async function readDir(dir) {
        const files = await fs.readdir(dir);

        var catDesc;

        if (!dir.endsWith("commands")) {
            try {
                catDesc = JSON.parse(
                    await fs.readFile(path.join(dir, catDescName))
                );
            } catch (err) {
                console.log("Couldn't read " + catDescName + " in " + dir);
            }
        }

        files.forEach(async f => {
            if (f == catDescName) return;

            const path = dir + "/" + f;

            const lstat = await fs.lstat(path);
            if (lstat.isDirectory()) return promises.push(readDir(path));

            if (!f.endsWith(".js"))
                return console.log(
                    f +
                        " is a file in the commands directory and is not a command."
                );

            const name = f.substring(0, f.length - 3);

            const trq = ".." + path.substring(1);
            const cat = path.substring(
                "./src/commands/".length,
                path.lastIndexOf("/")
            );

            const rq = require(trq);

            const obj = {
                ...rq,
                id: name,
                cat
            };

            load(obj, catDesc);
        });
    }

    promises.push(readDir("./src/commands"));

    await Promise.all(promises);
}

function alias(original, ...aliases) {
    aliases.forEach(a => {
        const newCmd = {
            alias: original
        };

        commands[a] = newCmd;

        if (!cats[commands[original].cat]) cats[commands[original].cat] = {};

        cats[commands[original].cat][a] = newCmd;
    });
}

async function run(msg, guildConfig, userConfig) {
    if (Date.now() - userConfig.lastMsg < cmdLimit)
        return console.log(msg.author.tag, "is too quick!");

    const split = msg.content.split(" ");
    const args = arrgv(split.slice(1).join(" "));

    args.raw = split.slice(1);

    var cmd =
        commands[
            split[0]
                .substr(
                    guildConfig.prefix &&
                        msg.content.startsWith(guildConfig.prefix)
                        ? guildConfig.prefix.length
                        : prefix.length
                )
                .toLowerCase()
        ];

    if (!cmd) return msg.channel.send("No command like that. :thinking:");

    if (cmd.alias) cmd = commands[cmd.alias.toLowerCase()];

    const exec = cmd.cmd;

    try {
        await exec(args, msg, guildConfig, userConfig);
        userConfig.lastMsg = Date.now();
    } catch (err) {
        msg.channel.send(
            createEmbed(
                "Error",
                "Something happened :flushed:\nSorry :heart:",
                {
                    name: "Name",
                    value: err.name
                },
                {
                    name: "Message",
                    value: err.message
                }
            )
        );
        console.log(err);
    }
}

loadAll();

module.exports = {
    run,
    commands,
    cats,
    getNameOfCommand,
    getDescOfCommand,
    getNameOfCat,
    getDescOfCat
};
