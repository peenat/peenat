const { prefix } = config;

const activities = [
    () => {
        return "My lucky number is " + parseInt(Math.random() * 100) + ".";
    },
    "Do you like peanuts?",
    "Nothing in the universe is random.",
    "Ya like jazz?",
    () => {
        const a = parseInt(Math.random() * 100);
        const b = parseInt(Math.random() * 100);

        return `${a} - ${b} is ${a - b}.`;
    },
    "These activities make no sense.",
    "Still looking for partners!",
    "Hi ❤️",
    "Shrek is love",
    "Shrek is life",
    "Shrek is shrexy",
    "Shreek",
    "Did somebody say onions?",
    "Wait, it's all ogre? Always has been."
];

const types = ["PLAYING", "LISTENING", "WATCHING"];

function random() {
    var activity = activities[parseInt(Math.random() * activities.length)];
    if (typeof activity == "function") activity = activity();

    return activity;
}

function change(bot) {
    bot.user.setActivity(`${prefix}help | ${random()}`, {
        type: types[parseInt(Math.random() * types.length)]
    });
}

module.exports = { activities, change, random };
