const fs = require("fs").promises;

const file = "./saves.json";

const loadLimit = 5000;
const saveLimit = 10000;

var lastLoad = 0;
var lastSave = 0;

var saves = {
    guilds: {},
    users: {}
};

function getGuildSave(id) {
    if (!saves.guilds[id]) saves.guilds[id] = {};
    return saves.guilds[id];
}

function getUserSave(id) {
    if (!saves.users[id]) saves.users[id] = {};
    return saves.users[id];
}

async function load(cb) {
    if (Date.now() - lastLoad >= loadLimit) {
        var data;

        try {
            data = await fs.readFile(file);
        } catch (err) {
            if (err.code == "ENOENT") {
                console.log(file + " doesn't exist, going to create it");
                await fs.writeFile("./saves.json", JSON.stringify(saves));
                return await load(cb);
            } else console.log(err);
        }

        console.log("Successfully loaded saves");
        saves = JSON.parse(data);

        lastLoad = Date.now();

        if (cb) cb();
    } else console.log("Didn't load saves due to load speed limit");
}

async function save(cb) {
    if (Date.now() - lastSave >= saveLimit) {
        await fs.writeFile(file, JSON.stringify(saves));
        console.log("Successfully Saved saves");
        lastSave = Date.now();
        if (cb) cb();
    } else console.log("Didn't save saves due to save speed limit");
}

function exitSave(opts, err) {
    save(() => {
        console.log("Saved on exit");

        if (err) console.log(err);

        if (opts.exit) process.exit();
    });
}

load();

process.on("exit", exitSave.bind(null, { cleanup: true }));

process.on("SIGINT", exitSave.bind(null, { exit: true }));

process.on("SIGUSR1", exitSave.bind(null, { exit: true }));
process.on("SIGUSR2", exitSave.bind(null, { exit: true }));

process.on("uncaughtException", exitSave.bind(null, { exit: true }));

module.exports = {
    file,
    saves,
    getGuildSave,
    getUserSave,
    load,
    save
};
